//+------------------------------------------------------------------+
//|                                                   MA Channel.mq4 |
//|                                                           Herval |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Herval"
#property link      "http://www.metaquotes.net"

#property indicator_chart_window
#property indicator_buffers 3
#property indicator_color1 Red
#property indicator_color2 Blue
#property indicator_color3 Blue

extern int MA_Period=10;
extern int MA_Shift=0;

//---- indicator buffers
double ExtMapBuffer[];
double ExtMapBufferTopLine[];
double ExtMapBufferBottomLine[];

//----
int ExtCountedBars=0;


int init()
  {
   int    draw_begin;
   string short_name;
//---- drawing settings
   IndicatorBuffers(3);
   SetIndexStyle(0,DRAW_LINE);
   SetIndexStyle(1,DRAW_LINE);
   SetIndexStyle(2,DRAW_LINE);
   SetIndexShift(0,MA_Shift);
   SetIndexShift(1,MA_Shift);
   SetIndexShift(2,MA_Shift);
   IndicatorDigits(MarketInfo(Symbol(),MODE_DIGITS));
   if(MA_Period<2) MA_Period=10;
   draw_begin=MA_Period-1;
//---- indicator short name
   short_name="EMA(";  
   draw_begin=0;
   IndicatorShortName(short_name+MA_Period+")");
   SetIndexDrawBegin(0,draw_begin);
   SetIndexDrawBegin(1,draw_begin);
   SetIndexDrawBegin(2,draw_begin);
//---- indicator buffers mapping
   SetIndexBuffer(0,ExtMapBuffer);
   SetIndexBuffer(1,ExtMapBufferTopLine);
   SetIndexBuffer(2,ExtMapBufferBottomLine);
//---- initialization done
   return(0);
  }


int deinit()
  {
//----
   
//----
   return(0);
  }

int start()
  {
   if(Bars<=MA_Period) return(0);
   ExtCountedBars=IndicatorCounted();
//---- check for possible errors
   if (ExtCountedBars<0) return(-1);
//---- last counted bar will be recounted
   if (ExtCountedBars>0) ExtCountedBars--;
//----
   ema();  
//---- done
   return(0);
  }

//+------------------------------------------------------------------+
//| Exponential Moving Average                                       |
//+------------------------------------------------------------------+
void ema()
  {
   double pr=2.0/(MA_Period+1);
   int    pos=Bars-2;
   if(ExtCountedBars>2) pos=Bars-ExtCountedBars-1;
//---- main calculation loop
   while(pos>=0)
     {
      if(pos==Bars-2) ExtMapBuffer[pos+1]=Close[pos+1];
      ExtMapBuffer[pos]=Close[pos]*pr+ExtMapBuffer[pos+1]*(1-pr);
      ExtMapBufferTopLine[pos]=ExtMapBuffer[pos]*1.02;
      ExtMapBufferBottomLine[pos]=ExtMapBuffer[pos]*0.98;
 	   pos--;
     }
  }


