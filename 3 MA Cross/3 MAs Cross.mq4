//+------------------------------------------------------------------+
//|                                                  3 MAs Cross.mq4 |
//|                                                           Herval |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Herval"
#property link      "http://www.metaquotes.net"

//#define tendencyMA 2400
#define shortMA 4
#define midMA 9
#define longMA 21
#define MAGICMA  666
#define stopPoints 400
#define takeProfitPoints 100
#define maxOrderNumber 5
#define orderStep 2 // step between orders
#define lotSize 0.1

double lastOrderPrice;
double stopLevel;
int orderCount;

//+------------------------------------------------------------------+
//| Calculate open positions                                         |
//+------------------------------------------------------------------+
int CalculateCurrentOrders(string symbol) {
   int buys=0,sells=0;
//----
   for(int i=0;i<OrdersTotal();i++) {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==MAGICMA) {
         if(OrderType()==OP_BUY)  buys++;
         if(OrderType()==OP_SELL) sells++;
      }
   }
//---- return orders volume
   if(buys > 0) {
      return(buys);
   } else { 
      return(-sells);
   }
}

//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit() {
}
  
//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int start() {
   // check for history and trading
   if(Bars<100 || IsTradeAllowed()==false) return;

   //lastOrderPrice = 0;
   orderCount = CalculateCurrentOrders(Symbol());
   //orderCount = MathAbs(orderCount);
//   Print(":::" + orderCount);
   //if (orderCount <= 5) {
      CheckForOpen();
   //}
   //CheckForClose();
   
   return(0);
}

//+------------------------------------------------------------------+
//| Check for close order conditions                                 |
//+------------------------------------------------------------------+
//void CheckForClose() {
//}

void CloseOrders(int type) {
   double price;

   for(int i=0;i<OrdersTotal();i++) {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==MAGICMA) {
         if(OrderType()==type) {
           if (OrderType() == OP_BUY) {
             price = Ask;
           } else {
             price = Bid;
           }
           
           OrderClose(OrderTicket(), OrderLots(), price, 3, Red);
         }
      }
   }
}

//+------------------------------------------------------------------+
//| Check for open order conditions                                  |
//+------------------------------------------------------------------+
void CheckForOpen() {
   double maShort, maMiddle, maLong, maTendency;
   bool signalBelow;
   double nextOrder;
   int res;
   
   // go trading only for first tiks of new bar
   if(Volume[0]>1) return;

   // get Moving Averages
   maShort = iMA(NULL,0,shortMA,0,MODE_SMA,PRICE_CLOSE,0);
   maMiddle = iMA(NULL,0,midMA,0,MODE_SMA,PRICE_CLOSE,0);
   maLong = iMA(NULL,0,longMA,0,MODE_SMA,PRICE_CLOSE,0);

//   maTendency = iMA(NULL,0,tendencyMA,0,MODE_SMA,PRICE_CLOSE,0);
   //signalBelow = (iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_MAIN,0)>iMACD(NULL,0,12,26,9,PRICE_CLOSE,MODE_SIGNAL,0));
   
   // sell conditions
   if(maLong > maMiddle && maMiddle > maShort/* && Bid > tendencyMA*/) {
       if (orderCount <= maxOrderNumber) {
          nextOrder = (lastOrderPrice - orderStep*Point);
          // open one more order if price moved down
          if (orderCount == 0 || MathAbs(Bid - nextOrder) > orderStep*Point ) {
//            CloseOrders(OP_BUY);
            //Print("Current orders: " + orderCount + " Balance: " + AccountEquity());
             //if (orderCount == 0) { // define the stop level for all the orders
               stopLevel = Bid+(stopPoints)*Point;
               //stopLevel = Ask-(stopPoints - (orderStep*orderCount))*Point;
             //}        
             lastOrderPrice = Bid;
             res = OrderSend(Symbol(),OP_SELL, GetLotSize(),Bid,3,stopLevel,Bid-takeProfitPoints*Point,"",MAGICMA,0,Red);
          }
       }
    }


   // buy conditions
   if(maShort > maMiddle && maMiddle > maLong/* && Ask < tendencyMA*/) {

       if (orderCount <= maxOrderNumber) {
          nextOrder = (lastOrderPrice - orderStep*Point);
          // open one more order if price moved down
          if (orderCount == 0 || MathAbs(Ask - nextOrder) > orderStep*Point ) {
//            CloseOrders(OP_SELL);
            //Print("Current orders: " + orderCount + " Balance: " + AccountEquity());
             //if (orderCount == 0) { // define the stop level for all the orders
               stopLevel = Ask-(stopPoints)*Point;
               //stopLevel = Ask-(stopPoints - (orderStep*orderCount))*Point;
             //}        
             lastOrderPrice = Ask;
             res = OrderSend(Symbol(),OP_BUY, GetLotSize(),Ask,3,stopLevel,Ask+takeProfitPoints*Point,"",MAGICMA,0,Red);
          }
       }
    }
}

double GetLotSize() {
 //return (AccountEquity()/10000);
 return(lotSize); 
  //return ((orderCount+1) * lotSize);
}