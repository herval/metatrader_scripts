//+------------------------------------------------------------------+
//|                                                    Close All.mq4 |
//|                                                           Herval |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
#property copyright "Herval"
#property link      "http://www.metaquotes.net"
#property show_confirm

// Close all orders on a given chart!
int start() {
   double price;
   bool res;
   
   for(int i = 0; i < OrdersTotal(); i++) {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false) break;
      if(OrderSymbol() == Symbol()) { Print("Order!");
           if (OrderType() == OP_BUY) {
             price = Ask;
           } else {
             price = Bid;
           }
           res = OrderClose(OrderTicket(), OrderLots(), price, 3, Red);
           if (!res) {
             Print("Error closing order ", GetLastError());
           }
      }
   }

   return(0);
}

